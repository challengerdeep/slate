## Exchanges

```shell
curl "https://api.kaiko.com/v1/history/exchanges?exchanges=okcoin,bitfinex,btc-e,bitstamp,coinbase,kraken,itbit,gemini,huobi&from=1230764400&pair=btcusd&fields=v,c"
```

```json
{
  "bitfinex": {
    "btcusd":
      [
        {"timestamp": 1455235200, "v": "2659", "c": "381.5"},
        {"timestamp": 1455148800, "v": "4651", "c": "377.99"}
      ]
  },
  "bitstamp": {
    "btcusd":
      [
        {"timestamp": 1455235200, "v": "2626", "c": "382.03"},
        {"timestamp": 1455148800, "v": "5407", "c": "377.98"}
      ]
  }
}
```

This endpoint returns all the trades which occured in the requested pair for the specified exchanges.
The API returns for each day the Bitcoin OHLC (open, high, low, close) values as well as the Bitcoin trading volume on this day.


### HTTP Request

`https://api.kaiko.com/v1/history/exchanges`

example:

`https://api.kaiko.com/v1/history/exchanges?exchanges=coinbase,kraken&pair=btcusd`
`https://api.kaiko.com/v1/history/exchanges?exchanges=okcoin,huobi&pair=btccny&fields=c,v`

### URL Parameters

Parameter | Description
----------|------------
from | get values from this timestamp
to | get values up to this timestamp
pair | get the prices for a specific pair
exchanges | get the values from these exchanges
fields | get the open, high, low, close values (use 'o', 'h', 'l', 'c')

The available exchanges / pairs are listed in the 'constants' section

examples:

`https://api.kaiko.com/v1/history/exchanges?exchanges=okcoin,bitfinex,btc-e,bitstamp,coinbase,kraken,itbit,gemini,huobi&from=1230764400&pair=btcusd&fields=v,c`
`https://api.kaiko.com/v1/history/exchanges?exchanges=huobi,okcoin&from=1230764400&pair=btccny&fields=v,c`

### Price indices keys and available currencies

| Name     | URL Key  | btcusd | btccny | btceur |
|----------|----------|--------|--------|--------|
| Kaiko Bitcoin Price Index      | global| ✓     |   ✓     | ✓     |
| Kaiko Bitcoin Price Index US   | us    | ✓     |   ✓     | ✓     |
| Kaiko Bitcoin Price Index EU   | eu    | ✓     |   ✓     | ✓     |
| Kaiko Bitcoin Price Index CN   | cn    | ✓     |   ✓     | ✓     |
