## Ticker

```json
{
   "btcusd":{
      "okcoin":{
         "last":230.91,
         "open":227.17,
         "high":235.09,
         "low":225.04,
         "vol24":5921.060000000003,
         "price24":231.7,
         "symbol":"btcusd",
         "exchange":"okcoin"
      },
      "kraken":{
         "last":228.66,
         "open":226.3,
         "high":233.85819,
         "low":223.1,
         "vol24":172.90313482999994,
         "price24":null,
         "symbol":"btcusd",
         "exchange":"kraken"
      },
      "bitstamp":{
         "last":230.46,
         "open":226.15,
         "high":231.96,
         "low":223.12,
         "vol24":12126.329888510005,
         "price24":230.43,
         "symbol":"btcusd",
         "exchange":"bitstamp"
      },
      "btc-e":{
         "last":228.666,
         "open":224.765,
         "high":230.5,
         "low":224.001,
         "vol24":6235.060544509995,
         "price24":228.178,
         "symbol":"btcusd",
         "exchange":"btc-e"
      },
      "bitfinex":{
         "last":231.21,
         "open":227.25,
         "high":233.95,
         "low":224.25,
         "vol24":8495.334984939997,
         "price24":231.93,
         "symbol":"btcusd",
         "exchange":"bitfinex"
      },
      "coinbase":{
         "last":231.31,
         "open":227.31,
         "high":232.5,
         "low":224.45,
         "vol24":6997.742131839992,
         "price24":231.73,
         "symbol":"btcusd",
         "exchange":"coinbase"
      }
   },
   "btceur":{
      "kraken":{
         "last":206.54,
         "open":202.73998,
         "high":208,
         "low":201.01,
         "vol24":3502.8192950799994,
         "price24":207.4,
         "symbol":"btceur",
         "exchange":"kraken"
      },
      "btc-e":{
         "last":203.1,
         "open":195,
         "high":206.441,
         "low":195,
         "vol24":138.27969475999998,
         "price24":202.077,
         "symbol":"btceur",
         "exchange":"btc-e"
      },
      "coinbase":{
         "last":206.59,
         "open":203.47,
         "high":208.07,
         "low":201.28,
         "vol24":1102.4373877200003,
         "price24":207.49,
         "symbol":"btceur",
         "exchange":"coinbase"
      }
   },
   "btccny":{
      "btcchina":{
         "last":1492.07,
         "open":1455.38,
         "high":1510,
         "low":1447.48,
         "vol24":13857.640299999985,
         "price24":1490,
         "symbol":"btccny",
         "exchange":"btcchina"
      },
      "okcoin":{
         "last":1493.84,
         "open":1455.34,
         "high":1536,
         "low":1445.84,
         "vol24":71999.86400000547,
         "price24":1493.07,
         "symbol":"btccny",
         "exchange":"okcoin"
      }
   }
}
```

Get the latest "ticker" information.

As Bitcoin exchanges don't have trading hours, the `open` time is set
to 00:00 UTC. Similarly, `high`/`low` values indicate the maximum and
minimum value between now and the start of the current day at 00:00
UTC

`vol24` is the exchanged volume between now and the same time
yesterday

`price24` is the price as it was at the same time yesterday

### HTTP Requests

All tickers:

`https://api.kaiko.com/v1/tickers`

All pairs for a specific exchange:

`https://api.kaiko.com/v1/tickers/{exchange}`

example:

`https://api.kaiko.com/v1/tickers/coinbase`

Specific exchange / pair ticker:

`https://api.kaiko.com/v1/tickers/{exchange}/{pair}`

example:

`https://api.kaiko.com/v1/tickers/coinbase/btcusd`
