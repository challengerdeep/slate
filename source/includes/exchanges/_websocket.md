## Websocket

### Overview

The websocket feed provides real-time market data updates for tickers,
orders and trades for Bitfinex, Bitstamp, BTCChina, Coinbase and
Huobi.

### Url

`wss://markets.kaiko.com:8080/v1`

### Subscribe

```json
{
  "type": "subscribe",
  "exchanges": [
    { "name": "bitfinex", "channels": ["ticker", "trades", "orderbook"] },
    { "name": "bitstamp", "channels": ["ticker", "trades", "orderbook"] },
    { "name": "btcchina", "channels": ["ticker", "trades", "orderbook"] },
    { "name": "coinbase", "channels": ["ticker", "trades", "orderbook"] },
    { "name": "huobi", "channels": ["ticker", "trades", "orderbook"] }
  ]
}
```

To begin receiving feed messages, you must first send a subscribe
message to the server indicating which exchanges and channels you wish
to subscribe to.

The message must be JSON encoded.

### Unsubscribe

```json
{
  "type": "unsubscribe",
  "exchanges": [
    { "name": "bitfinex", "channels": ["ticker", "trades", "orderbook"] },
    { "name": "bitstamp", "channels": ["ticker", "trades", "orderbook"] },
    { "name": "btcchina", "channels": ["ticker", "trades", "orderbook"] },
    { "name": "coinbase", "channels": ["ticker", "trades", "orderbook"] },
    { "name": "huobi", "channels": ["ticker", "trades", "orderbook"] }
  ]
}
```

To stop receiving messages from given channels of an exchange, send an
unsubcribe message.

The message must be JSON encoded.
