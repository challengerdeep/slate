## Constants

### Exchanges keys and available pairs

| Name     | URL Key  | btcusd | btccny | btceur |
|----------|----------|--------|--------|--------|
| BTC-e    | btc-e    | ✓     |        | ✓      |
| BTCC     | btcchina |       | ✓      |        |
| Bitfinex | bitfinex | ✓     |        |        |
| Bitstamp | bitstamp | ✓     |        |        |
| Gemini   | gemini   | ✓     |        |        |
| Coinbase | coinbase | ✓     |        | ✓      |
| Kraken   | kraken   | ✓     |        | ✓      |
| Itbit    | itbit    | ✓     |        | ✓      |
| OKCoin   | okcoin   | ✓     | ✓      |        |
| Huobi    | huobi    | ✓     | ✓      |        |
