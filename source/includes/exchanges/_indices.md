## Indices

```shell
curl "https://api.kaiko.com/v1/history/indices?indices=global,cn,us,eu&from=1230764400&currency=usd&fields=v,c"
```

```json
[
  {
    "global": [
      {
        "timestamp": 1455235200,
        "v": "505973.96271378116",
        "c": "387.73"
      }
    ],
    "cn": [
      {
        "timestamp": 1455235200,
        "v": "472125.7518050194",
        "c": "394.17"
      }
    ],
    "eu": [
      {
        "timestamp": 1455235200,
        "v": "3826.2436363999986",
        "c": "383.31"
      }
    ],
    "global": [
      {
        "timestamp": 1455235200,
        "v": "505973.96271378116",
        "c": "387.73"
      }
    ],
    "us": [
      {
        "timestamp": 1455235200,
        "v": "30021.96727236188",
        "c": "381.46"
      }
    ]
  }
]
```

Get our daily price indices value history. We currently offer 2 types of price index:
- A global one, reflecting the Bitcoin price worldwide
- Regional ones (US, EU and CN) reflecting the Bitcoin price on a regional level. Even if these indices reflect the price of a specific trading area, they are available in multiple currency to be benchmarked against each other.

The API returns for each day the Bitcoin OHLC (open, high, low, close) values as well as the Bitcoin trading volume on this day.

### HTTP Request

`https://api.kaiko.com/v1/history/indices`

example:

`https://api.kaiko.com/v1/history/indices?indices=global`
`https://api.kaiko.com/v1/history/indices?indices=global,us&currency=cny&fields=o,h,l,c`

### URL Parameters

Parameter | Description
----------|------------
from | get values from this timestamp
to | get values up to this timestamp
currency* | all indices can be returned in dollar, euro or yuan (use 'usd', 'eur', 'cny')
indices | query our global index or the regional ones (available values: 'global', 'us', 'eu', 'cn')
fields | get the open, high, low, close values (use 'o', 'h', 'l', 'c')

* All indices values are computed and stored in USD but we offer the possibility to convert the values in euro and yuan using the European Central Bank rates.

example:

`https://api.kaiko.com/v1/index/global/btcusd?from=1443018722000&to=1443018734000`

### Price indices keys and available currencies

| Name     | URL Key  | btcusd | btccny | btceur |
|----------|----------|--------|--------|--------|
| Kaiko Bitcoin Price Index      | global| ✓     |   ✓     | ✓     |
| Kaiko Bitcoin Price Index US   | us    | ✓     |   ✓     | ✓     |
| Kaiko Bitcoin Price Index EU   | eu    | ✓     |   ✓     | ✓     |
| Kaiko Bitcoin Price Index CN   | cn    | ✓     |   ✓     | ✓     |
