## Trades

```json
[
   {
      "timeuuid":"8ecb2900-61fc-11e5-80f3-6070bb1edc17",
      "timestamp":"1443017306000",
      "id":"11864299",
      "price":231.15,
      "amount":0.11,
      "sell":true,
      "symbol":"btcusd"
   },
   {
      "timeuuid":"ee750480-61fb-11e5-8062-da70339da905",
      "timestamp":"1443017037000",
      "id":"11864295",
      "price":231.15,
      "amount":0.01020154,
      "sell":true,
      "symbol":"btcusd"
   }
]
```

Get a range of trades from a specific exchange / pair

### HTTP Request

`https://api.kaiko.com/v1/trades/{exchange}/{pair}`

example:

`https://api.kaiko.com/v1/trades/bitfinex/btcusd`

### URL Parameters

Parameter | Description
----------|------------
from | get trades from this timestamp
to | get trades up to this timestamp
page | get the next result page - more on this <a href='#pagination'>here</a>

example:

`https://api.kaiko.com/v1/trades/bitfinex/btcusd?from=1443018722000&to=1443018734000`

## Trade by ID

```json
[
   {
      "exchange":"bf",
      "id":"11864403",
      "symbol":"btcusd",
      "amount":0.01,
      "date":"2015-09-23T14:32:02.000Z",
      "date_uuid":"dacb9d00-61ff-11e5-997f-3ddaa0d82a4b",
      "day_hour":"2015-09-23T14:00:00.000Z",
      "price":231.26,
      "sell":true
   }
]
```

Get a specific trade using the exchange own ID

### HTTP Request

`https://api.kaiko.com/v1/trades/{exchange}/{pair}/{id}`

example:

`https://api.kaiko.com/v1/trades/bitfinex/btcusd/11864403`
