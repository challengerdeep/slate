# Exchanges

Retrieve tickers, trades and order books from the major exchanges and
the Kaiko Bitcoin price index

All the data is available through both a REST and a Websocket API
(when available).
