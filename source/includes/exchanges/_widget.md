## Widget

### Overview

The ticker widget provides an embeddable view of the current and past Bitcoin Price. It can be added for free to any website or blog with a simple line of code.
It is highly customizable and can be added in the form of an ```iframe``` or via ```inline javascript```.

### Format

To add the widget as an ```iframe```, simply use the following markup:
```html
<iframe src="https://cdn.kaiko.com/js/iframe.html" style='border: none'></iframe>
```

To use the widget via inline javascript, prefer the following markup:
```html
<div id="bitcoin-widget"></div>
<script src="https://cdn.kaiko.com/js/widget.min.js"></script>
```

### Customizations

To customize the widgets, 2 options are available:
- If you are using the iframe: add query strings parameter to the source url. For example, to select the size, use the following source:
```html
https://cdn.kaiko.com/js/iframe.html?size=small&source=cn
```
```html
https://cdn.kaiko.com/js/iframe.html?size=mpu&pair=btceur
```
- If you are using inline javascript: add date attributes to your ```div```. For example, to select the size, use the following source:
```html
<div id="bitcoin-widget" data-size="small" data-align="center" data-theme="dark" data-pair="btcusd" data-source="global"></div>
```

#### Size

2 sizes are available:
- a 210x130 small widget available with ```data-size="small"```
- a 300x250 large widget available with ```data-size="mpu"``` (minimum publishing unit)

Please note that, if you are using an iframe with the minimum publishing unit format (mpu), you need to adapt the iframe height to 250px:
```html
<iframe src="https://cdn.kaiko.com/js/iframe.html?size=mpu" style='height: 250px; border: none'></iframe>
```


#### Data sources

All of our indices are available in the widget:
- The Kaiko Bitcoin Price Index (reflects the Bitcoin Price across all major exchanges) - use it with ```data-source=global```
- The Kaiko Bitcoin Price Index CNY (reflects the Bitcoin Price across all major exchanges trading in CN) - use it with ```data-source=cn```
- The Kaiko Bitcoin Price Index USD (reflects the Bitcoin Price across all major exchanges trading in US) - use it with ```data-source=us```
- The Kaiko Bitcoin Price Index EUR (reflects the Bitcoin Price across all major exchanges trading in EU) - use it with ```data-source=eu```

More information on our indices and their computation is available <a href='https://www.kaiko.com/bitcoin-price' traget='_blank'>here</a>

#### Currency

The widget can be used to show the price in USD, CNY or EUR. All of our indices are converted in the selected currency using the European Central Bank rates (available <a href='https://www.ecb.europa.eu/stats/exchange/eurofxref/html/index.en.html' target='_blank'>here</a>)

Customize the currency as follows:
```html
<div id="bitcoin-widget" data-pair="btcusd"></div>
<div id="bitcoin-widget" data-pair="btccny"></div>
<div id="bitcoin-widget" data-pair="btceur"></div>
```

Note that the default currency is always ```btcusd```, even if you chose to display the Kaiko Bitcoin Index CN.

#### Theme

2 color themes are available for now

```html
<iframe src="https://cdn.kaiko.com/js/iframe.html?theme=dark" style='border: none'></iframe>
```
```html
<iframe src="https://cdn.kaiko.com/js/iframe.html?theme=blue" style='border: none'></iframe>
```

#### Alignment

The widget can be aligned ```left```, ```right``` or ```center```

```html
<div id="bitcoin-widget" data-align="left"></div>
<div id="bitcoin-widget" data-align="right"></div>
<div id="bitcoin-widget" data-align="center"></div>
```
