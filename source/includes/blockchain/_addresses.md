## Address balance

```json
{
   "address":"3Nt1smucEdFks8uYQhyGvXGBuocTcMSmsT",
   "balance":"10000000000",
   "sent":"35000500000",
   "received":"45000500000",
   "sent_count":"4",
   "received_count":"5",
   "first_tx":"0dac3eab779271cc20391530b193302fbdb38b6ff878cef8a807435e166cec52",
   "first_seen":"2015-06-03T20:39:09.000Z",
   "last_tx":"65822118727e28ddb6e5ce48b09ba52e3fb91414dca03b4c9d8c16ec91e876a3",
   "last_seen":"2015-09-23T16:16:40.000Z",
   "unconfirmed_balance":"0",
   "unconfirmed_sent":"0",
   "unconfirmed_received":"0",
   "unconfirmed_received_count":"2",
   "unconfirmed_sent_count":"0"
}
```

This endpoint retrieves an address balance. The returned balance only
accounts for confirmed transaction. Please note that it also takes
multi-sig transactions into accounts.

### HTTP Request

`https://api.kaiko.com/v1/addresses/{wip_address|hash160}`

example:

`https://api.kaiko.com/v1/addresses/3Nt1smucEdFks8uYQhyGvXGBuocTcMSmsT`

## Address transactions history

```json
{
   "address":"3Nt1smucEdFks8uYQhyGvXGBuocTcMSmsT",
   "balance":"10000000000",
   "sent":"35000500000",
   "received":"45000500000",
   "sent_count":"4",
   "received_count":"5",
   "first_tx":"0dac3eab779271cc20391530b193302fbdb38b6ff878cef8a807435e166cec52",
   "first_seen":"2015-06-03T20:39:09.000Z",
   "last_tx":"65822118727e28ddb6e5ce48b09ba52e3fb91414dca03b4c9d8c16ec91e876a3",
   "last_seen":"2015-09-23T16:16:40.000Z",
   "unconfirmed_balance":"0",
   "unconfirmed_sent":"0",
   "unconfirmed_received":"0",
   "unconfirmed_received_count":"2",
   "unconfirmed_sent_count":"0",
   "confirmed_transactions":[
      {
         "hash":"65822118727e28ddb6e5ce48b09ba52e3fb91414dca03b4c9d8c16ec91e876a3",
         "block_hash":"00000000000000000ff0d004c9d46d06bee1ee455a0e3a6ad83754c7aefd0142",
         "block_time":"2015-09-23T16:16:40.000Z",
         "amount":"10383552283",
         "fees":"0",
         "lock_time":0,
         "inputs":[
            {
               "addresses":[
                  "3NqnfiqKjeTvUYJCa5zfpHZhVkRAg1qfAe"
               ],
               "amount":"33399628"
            },
            {
               "addresses":[
                  "39p9HqgF1giRX6uk9Lbfi473SVYsTjzis5"
               ],
               "amount":"49000000"
            },
            {
               "addresses":[
                  "3FgD9zFS7Rqf5rbPzVc5KxQCBq95atVqyX"
               ],
               "amount":"49095004"
            },
            {
               "addresses":[
                  "32b5pMnVG1qKvVwCR9SCQcKCNv55ma6xsW"
               ],
               "amount":"181293000"
            },
            {
               "addresses":[
                  "3HjwjF7yFR4Nx3pECiCceeJjkaPYBJBoDF"
               ],
               "amount":"70795000"
            },
            {
               "addresses":[
                  "3Nt1smucEdFks8uYQhyGvXGBuocTcMSmsT"
               ],
               "amount":"10000000000"
            }
         ],
         "outputs":[
            {
               "addresses":[
                  "1Jrin1FByC2EW1jWPmYQzwvX3CTWRR7dhE"
               ],
               "amount":"20060000",
               "script_type":"pubkeyhash",
               "spent":null
            },
            {
               "addresses":[
                  "3F32zTWBG1bRFTcveV8wsTxe1AL4uexBj7"
               ],
               "amount":"802400",
               "script_type":"scripthash",
               "spent":null
            },
            {
               "addresses":[
                  "1EChY3xbqHA7SLVzZ1HU72cn4qSRLrj5YR"
               ],
               "amount":"155104890",
               "script_type":"pubkeyhash",
               "spent":null
            },
            {
               "addresses":[
                  "12h5vGY4dQZQv8ZBSnGciz4soFCuRXzhDL"
               ],
               "amount":"39322087",
               "script_type":"pubkeyhash",
               "spent":null
            },
            {
               "addresses":[
                  "32gMHLohmuNA2zeY6vESsd2NqaAc48Vaoh"
               ],
               "amount":"5462985469",
               "script_type":"scripthash",
               "spent":true
            },
            {
               "addresses":[
                  "1Bdx7PUY2zL9j47CVX4PMnCbjArKSn4bdp"
               ],
               "amount":"200000000",
               "script_type":"pubkeyhash",
               "spent":null
            },
            {
               "addresses":[
                  "1FQBqWvxwmkmTataSAAa9RpYMV5g9DcMph"
               ],
               "amount":"3000631353",
               "script_type":"pubkeyhash",
               "spent":null
            },
            {
               "addresses":[
                  "1KZmZmVqscyFh4Y3yDCJtExMh4L8LBu6SC"
               ],
               "amount":"108056399",
               "script_type":"pubkeyhash",
               "spent":null
            },
            {
               "addresses":[
                  "1JLEAbcKxuf83tQeBAc58RQu6jFPqfPvg6"
               ],
               "amount":"100000000",
               "script_type":"pubkeyhash",
               "spent":null
            },
            {
               "addresses":[
                  "19GQ1CePFBinD1B8F9X886M6rVdBybHnRR"
               ],
               "amount":"851670685",
               "script_type":"pubkeyhash",
               "spent":null
            },
            {
               "addresses":[
                  "18eawSxdQakWBkjDn4vTqZb2LE1wkVYTLR"
               ],
               "amount":"239520000",
               "script_type":"pubkeyhash",
               "spent":null
            },
            {
               "addresses":[
                  "18hrAmYRD8nLEqNFvHaqb33wBHEGumbGGd"
               ],
               "amount":"205399000",
               "script_type":"pubkeyhash",
               "spent":null
            }
         ],
         "balance_update":"-10000000000"
         }
   ],
  "unconfirmed_transaction": []
}
```

This endpoint retrieves an address' transactions. Note that you can
sort the sent/received transactions adding 0 for sent, 1 for received
as filter in the query options.

### HTTP Request

`https://api.kaiko.com/v1/addresses/{wip_address|hash160}/transactions`

example:

`https://api.kaiko.com/v1/addresses/3Nt1smucEdFks8uYQhyGvXGBuocTcMSmsT/transactions`
