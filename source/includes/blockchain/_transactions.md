## Transactions

```shell
curl "https://api.kaiko.com/v1/transactions/51eb0b1e85c7d6bf6bc7290450db41998d5442da2426c599ffa1ad259cdb5d26"
```

```json
{
    "amount": "350111486",
    "block_hash": "00000000000000000fa8bfd95b6a473d5d8864c74167dfec7077f684b95d106c",
    "block_height": 375479,
    "block_time": "2015-09-21T15:25:48.000Z",
    "confirmations": 6,
    "fees": "0",
    "hash": "51eb0b1e85c7d6bf6bc7290450db41998d5442da2426c599ffa1ad259cdb5d26",
    "inputs": [
        {
            "addresses": [
                "1NkaiuAxC8uLkzZW9CciTKTusfKjqPiivE"
            ],
            "amount": "350211486",
            "index": 0,
            "output_index": 1,
            "previous_transaction_hash": "f7579657711573ec30e8200bb1661b1be8dc8f9ded79a54c6a85d3feec6181ac",
            "script": "304402200af4d49d8b41539709f6e4a09149d51cead0f3820f35125a3b814d93482d9b7502204ed5f9e26b3001579d2a1e2e629c36109ac9b45992e9091806a092e54b5d6b0301 0455d0b7d69a93d520540a33e029cb5ae1e5a58858339297dfc3972621fff38c530ace5b995a42bfccdcf56c87a273cf3eb9bbd6428875510164e67c3511bac9dd",
            "script_hex": "47304402200af4d49d8b41539709f6e4a09149d51cead0f3820f35125a3b814d93482d9b7502204ed5f9e26b3001579d2a1e2e629c36109ac9b45992e9091806a092e54b5d6b0301410455d0b7d69a93d520540a33e029cb5ae1e5a58858339297dfc3972621fff38c530ace5b995a42bfccdcf56c87a273cf3eb9bbd6428875510164e67c3511bac9dd",
            "script_type": "pubkeyhash"
        }
    ],
    "lock_time": 0,
    "nbInputs": 1,
    "nbOutputs": 2,
    "outputs": [
        {
            "addresses": [
                "1Q2gFiucbP2oRyh5Y5P2i4nzwwPEnKv4Md"
            ],
            "amount": "50000000",
            "index": 0,
            "script": "OP_DUP OP_HASH160 fc9c1391abc1391d4566efc660fae66a67ec983e OP_EQUALVERIFY OP_CHECKSIG",
            "script_hex": "76a914fc9c1391abc1391d4566efc660fae66a67ec983e88ac",
            "script_type": "pubkeyhash",
            "spent": null
        },
        {
            "addresses": [
                "1NkaiuAxC8uLkzZW9CciTKTusfKjqPiivE"
            ],
            "amount": "300111486",
            "index": 1,
            "script": "OP_DUP OP_HASH160 ee98ad8867a87d1a1a4a6b3d02ec83f56d6c46dd OP_EQUALVERIFY OP_CHECKSIG",
            "script_hex": "76a914ee98ad8867a87d1a1a4a6b3d02ec83f56d6c46dd88ac",
            "script_type": "pubkeyhash",
            "spent": true
        }
    ],
    "prev_hash": "f7579657711573ec30e8200bb1661b1be8dc8f9ded79a54c6a85d3feec6181ac",
    "size": 257,
    "version": 1
}
```

This endpoint retrieves a specific transaction by its height. Works for
unconfirmed transactions too. This query is paginated by inputs and
outputs.

### HTTP Request

`GET https://api.kaiko.com/v1/transactions/{hash}`

### URL Parameters

Parameter | Description
--------- | -----------
hash | The transaction hash in big endian
from | Starting input/output index (included)
to | Ending input/output index (excluded)
