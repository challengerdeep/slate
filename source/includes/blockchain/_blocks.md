## Blocks

```shell
curl "https://api.kaiko.com/v1/blocks/300000"
```

```json
{
   "hash":"000000000000000082ccf8f1557c5d40b21edabb18d2d691cfbf87118bac7254",
   "next_block_hash":"000000000000000049a0914d83df36982c77ac1f65ade6a52bdced2ce312aba9",
   "previous_block_hash":"000000000000000067ecc744b5ae34eebbde14d21ca4db51652e4d67e155f07e",
   "merkleroot":"915c887a2d9ec3f566a648bedcf4ed30d0988e22268cfe43ab5b0cf8638999d3",
   "branch":"main",
   "fees":"4028360",
   "value":"208005436605",
   "nonce":222771801,
   "bits":419465580,
   "difficulty":"8000872135",
   "reward":"2500000000",
   "transactions_count":237,
   "size":128810,
   "height":300000,
   "timestamp":"2014-05-10T06:32:34.000Z",
   "version":2,
   "total_out":"208005436605",
   "transactions":[
      {
         "hash":"b39fa6c39b99683ac8f456721b270786c627ecb246700888315991877024b983",
         "value":"2504028360",
         "index":0
      },
      {
         "hash":"7301b595279ece985f0c415e420e425451fcf7f684fcce087ba14d10ffec1121",
         "value":"36472990000",
         "index":1
      },
      {
         "hash":"6961d06e4a921834bbf729a94d7ab423b18ddd92e5ce9661b7b871d852f1db74",
         "value":"4534776015",
         "index":2
      },
      {
         "hash":"85e72c0814597ec52d2d178b7125af0e3cfa07821912ca81bf4b1fbe4b4b70f2",
         "value":"53399980000",
         "index":3
      }
    ],
    "transactions_count": 237,
    "value": "208005436605",
    "version": 2
}
```

This endpoint retrieves a specific block by its hash or height.

### HTTP Request

`GET https://api.kaiko.com/v1/blocks/{hash|height}`

### URL Parameters

Parameter | Description
--------- | -----------
hash | The block hash in big endian
height | The block height

## Multiple blocks

```shell
curl "https://api.kaiko.com/v1/blocks?from=1&to=4"
```

```json
[
    {
        "branch": "main",
        "confirmations": 374910,
        "hash": "00000000839a8e6886ab5951d76f411475428afc90947ee320161bbf18eb6048",
        "height": 1,
        "miner": null,
        "size": 215,
        "timestamp": "2009-01-09T02:54:25.000Z",
        "transactions_count": 1,
        "value": "5000000000"
    },
    {
        "branch": "main",
        "confirmations": 374909,
        "hash": "000000006a625f06636b8bb6ac7b960a8d03705d1ace08b1a19da3fdcc99ddbd",
        "height": 2,
        "miner": null,
        "size": 215,
        "timestamp": "2009-01-09T02:55:44.000Z",
        "transactions_count": 1,
        "value": "5000000000"
    },
    {
        "branch": "main",
        "confirmations": 374908,
        "hash": "0000000082b5015589a3fdf2d4baff403e6f0be035a5d9742c1cae6295464449",
        "height": 3,
        "miner": null,
        "size": 215,
        "timestamp": "2009-01-09T03:02:53.000Z",
        "transactions_count": 1,
        "value": "5000000000"
    }
]
```

This endpoint retrieves a number of blocks, paginated by their height.
The `to` parameter is always excluded from the response.

### HTTP Request

`GET https://api.kaiko.com/v1/blocks?from=1&to=4`

### URL Parameters

Parameter | Description
--------- | -----------
from | The starting height (included)
to | The ending height (excluded)
order | The sorting order. Either `asc` or `desc`
full | Boolean. Include transactions list or not.
limit | The number of blocks per page. Max is 20.

### Latest blocks

When you do not give any `from` or `to` parameters, this endpoint
retrieves a list of 20 latest blocks.

## Block transactions

```shell
curl "https://api.kaiko.com/v1/blocks/300000"
```

```json
{
  "hash": "000000000000000082ccf8f1557c5d40b21edabb18d2d691cfbf87118bac7254",
  "next_block_hash": "000000000000000049a0914d83df36982c77ac1f65ade6a52bdced2ce312aba9",
  "previous_block_hash": "000000000000000067ecc744b5ae34eebbde14d21ca4db51652e4d67e155f07e",
  "merkleroot": "915c887a2d9ec3f566a648bedcf4ed30d0988e22268cfe43ab5b0cf8638999d3",
  "branch": "main",
  "fees": "4028360",
  "value": "208005436605",
  "nonce": 222771801,
  "bits": 419465580,
  "difficulty": "8000872135",
  "reward": "2500000000",
  "transactions_count": 237,
  "size": 128810,
  "height": 300000,
  "timestamp": "2014-05-10T06:32:34.000Z",
  "version": 2,
  "total_out": "208005436605",
  "transactions": [
    {
      "hash": "b39fa6c39b99683ac8f456721b270786c627ecb246700888315991877024b983",
      "block_height": 300000,
      "block_hash": "000000000000000082ccf8f1557c5d40b21edabb18d2d691cfbf87118bac7254",
      "block_time": "2014-05-10T06:32:34.000Z",
      "lock_time": 0,
      "size": 157,
      "version": 1,
      "fees": "0",
      "prev_hash": "0000000000000000000000000000000000000000000000000000000000000000",
      "inputs": [
        {
          "previous_transaction_hash": "0000000000000000000000000000000000000000000000000000000000000000",
          "output_index": 4294967295,
          "amount": "2500000000",
          "script_hex": "03e09304062f503253482f0403c86d53087ceca141295a00002e522cfabe6d6d7561cf262313da1144026c8f7a43e3899c44f6145f39a36507d36679a8b700610400000000000000",
          "script_type": null,
          "addresses": []
        }
      ],
      "outputs": [
        {
          "amount": "2504028360",
          "script": "OP_DUP OP_HASH160 80ad90d403581fa3bf46086a91b2d9d4125db6c1 OP_EQUALVERIFY OP_CHECKSIG",
          "script_hex": "76a91480ad90d403581fa3bf46086a91b2d9d4125db6c188ac",
          "script_type": "pubkeyhash",
          "spent": true,
          "addresses": [
            "1CjPR7Z5ZSyWk6WtXvSFgkptmpoi4UM9BC"
          ]
        }
      ],
      "amount": "2504028360",
      "index": 0
    },
    {
      "hash": "7301b595279ece985f0c415e420e425451fcf7f684fcce087ba14d10ffec1121",
      "block_height": 300000,
      "block_hash": "000000000000000082ccf8f1557c5d40b21edabb18d2d691cfbf87118bac7254",
      "block_time": "2014-05-10T06:32:34.000Z",
      "lock_time": 0,
      "size": 259,
      "version": 1,
      "fees": "0",
      "prev_hash": "e9166ef24d3bb23a5af806a35e2b4924d325ddc655d7482e6716eedc5040ff4d",
      "inputs": [
        {
          "previous_transaction_hash": "e9166ef24d3bb23a5af806a35e2b4924d325ddc655d7482e6716eedc5040ff4d",
          "output_index": 0,
          "amount": "36473000000",
          "script": "3046022100cb6dc911ef0bae0ab0e6265a45f25e081fc7ea4975517c9f848f82bc2b80a909022100e30fb6bb4fb64f414c351ed3abaed7491b8f0b1b9bcd75286036df8bfabc3ea501 04b70574006425b61867d2cbb8de7c26095fbc00ba4041b061cf75b85699cb2b449c6758741f640adffa356406632610efb267cb1efa0442c207059dd7fd652eea",
          "script_hex": "493046022100cb6dc911ef0bae0ab0e6265a45f25e081fc7ea4975517c9f848f82bc2b80a909022100e30fb6bb4fb64f414c351ed3abaed7491b8f0b1b9bcd75286036df8bfabc3ea5014104b70574006425b61867d2cbb8de7c26095fbc00ba4041b061cf75b85699cb2b449c6758741f640adffa356406632610efb267cb1efa0442c207059dd7fd652eea",
          "script_type": "pubkeyhash",
          "addresses": [
            "18heVg1RMgPbrciP2iW42nfsTtyPrMhpkd"
          ]
        }
      ],
      "outputs": [
        {
          "amount": "10500000000",
          "script": "OP_DUP OP_HASH160 61cf5af7bb84348df3fd695672e53c7d5b3f3db9 OP_EQUALVERIFY OP_CHECKSIG",
          "script_hex": "76a91461cf5af7bb84348df3fd695672e53c7d5b3f3db988ac",
          "script_type": "pubkeyhash",
          "spent": true,
          "addresses": [
            "19vAwujzTjTzJhQQtdQFKeP5u3msLusgWs"
          ]
        },
        {
          "amount": "25972990000",
          "script": "OP_DUP OP_HASH160 fd4ed114ef85d350d6d40ed3f6dc23743f8f99c4 OP_EQUALVERIFY OP_CHECKSIG",
          "script_hex": "76a914fd4ed114ef85d350d6d40ed3f6dc23743f8f99c488ac",
          "script_type": "pubkeyhash",
          "spent": null,
          "addresses": [
            "1Q6NNpHM1pyh6kEqzinBhEgsRc3nmpTGLm"
          ]
        }
      ],
      "amount": "36472990000",
      "index": 1
    },
```

Get detailed transactions (inputs and outputs scripts, addresses and
amount) for a specific block. This query is paginated by transaction
index.

### HTTP Request

`GET https://api.kaiko.com/v1/blocks/{hash|height}/transactions`

### URL Parameters

Parameter | Description
--------- | -----------
hash | The block hash in big endian
height | The block height
from | Starting index (included)
to | Ending index (excluded)
