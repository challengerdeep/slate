## Pagination

Requests that return multiple items will be paginated by
default, using the `page` query string parameter. The unit of `page`
depends on the requested resource.

The `from` parameter is always included in the response. The `to` parameter is always
excluded in the response.

### Example

`https://api.kaiko.com/v1/trades/coinbase/btcusd?from=1438764481000&to=1438764881000`

### Link Header

The pagination info is included in [the Link
header](https://tools.ietf.org/html/rfc5988). It is easier to follow these Link header values instead of constructing your own URLs.

`Link:
<https://api.kaiko.com/v1/trades/coinbase/btcusd?page=&from=1438764481000&to=1438764881000>;
rel="next"`
