# API

## Introduction

Welcome to the Kaiko API! You can use our API to request blockchain and exchanges data.

All requests and responses are `application/json` content type and follow typical HTTP response status codes for success and failure.

### HTTP endpoint

`https://api.kaiko.com/v1`

### Websocket endpoint

`wss://markets.kaiko.com/v1`
